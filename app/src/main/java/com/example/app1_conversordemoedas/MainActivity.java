package com.example.app1_conversordemoedas;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private ViewHolder mViewHolder = new ViewHolder();
    private static SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.mViewHolder.textToday = findViewById(R.id.text_today);
        this.mViewHolder.textToday.setText(SIMPLE_DATE_FORMAT.format(Calendar.getInstance().getTime()));
            this.mViewHolder.editValue = findViewById(R.id.edit_value); // chama caixa de texto
            this.mViewHolder.textDolar = findViewById(R.id.text_dolar); // caixa que retorna resposta para usuario
            this.mViewHolder.textEuro = findViewById(R.id.text_Euro); // caixa que retorna resposta para usuario

            this.mViewHolder.ButtonCalcular = findViewById(R.id.button_calcular); // Funcao para o botao funcionar
            this.mViewHolder.ButtonCalcular.setOnClickListener(this);
            this.clearValues(); // Zerar caixas de texto que retorna a resposta
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.button_calcular) {
            String value = this.mViewHolder.editValue.getText().toString();
            if ("".equals(value)) {
// mostra uma mensagem de erro
                Toast.makeText(this, this.getString(R.string.informe_um_valor), Toast.LENGTH_LONG).show();
            } else {
                Double real = Double.valueOf(value);
                this.mViewHolder.textDolar.setText(String.format("%.2f",(real / 4)));
                this.mViewHolder.textEuro.setText(String.format("%.2f",(real / 5)));

            }
        }
    }


    private void clearValues() {
        this.mViewHolder.textEuro.setText("");
        this.mViewHolder.textDolar.setText("");


    }

    private static class ViewHolder {
        EditText editValue;
        TextView textDolar;
        TextView textEuro;
        Button ButtonCalcular;
        TextView textToday;

    }
}